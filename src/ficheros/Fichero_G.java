/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheros;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.DefaultListModel;

/**
 *
 * @author pedroluis
 */

//manipulaci�n ddl base de datos y tablas
public class Fichero_G {
    public static long tama�o_cabecera = 8; //bytes - longitud fija
    public static long tama�o_registro = 40;//bytes - longitud fija
    
    public static int crear(File fichero){
        if(fichero.exists()){
            return 2;
        }
        Path path = Paths.get(fichero.getAbsolutePath());
        RandomAccessFile flujo;
        try {
            Files.createFile(path);
            flujo = new RandomAccessFile(fichero, "rw");
            Cabecera c = new Cabecera();
            c.numero_registros = 0;
            c.numero_vacios = 0;
            flujo.writeInt(c.numero_registros);
            flujo.writeInt(c.numero_vacios);
            //System.out.println("Tama�o en bytes: " + flujo.length());
            flujo.close();
        } catch (IOException ex) {
            return 2;
        }
        return 1;
    }
    public static int crear_vacio(File fichero){
        if(fichero.exists()){
            return 0;
        }
        Path path = Paths.get(fichero.getAbsolutePath());
        try {
            Files.createFile(path);
        } catch (IOException ex) {
            return 2;
        }
        return 1;
    }
    public static int escribirRegistro(File f, Registro registro){
        RandomAccessFile flujo;
        try {
            flujo = new RandomAccessFile(f, "rw");
            flujo.seek(0);
            Cabecera c = new Cabecera();
            c.numero_registros = flujo.readInt();
            c.numero_vacios = flujo.readInt();
            registro.estado = 1;
            long pos = 0;
            
            if(c.numero_vacios == 0){
                registro.nro = c.numero_registros;
                pos = tama�o_cabecera + c.numero_registros*(tama�o_registro);
            }
            else{
               int i = 0, numR = Fichero_G.numeroRegistros(flujo);
               int estado;
               while(i < numR){
                   flujo.seek(tama�o_cabecera + i*(tama�o_registro));
                   flujo.readInt();
                   flujo.readUTF();
                   estado = flujo.readInt();
                   
                   if(estado == 0){
                      registro.nro = i;
                      pos = tama�o_cabecera + i*(tama�o_registro);
                      c.numero_vacios--;
                      break;
                   }
                   i++;
               }
            }
            
            flujo.seek(pos);
            flujo.writeInt(registro.nro);
            flujo.writeUTF(registro.item);
            flujo.writeInt(registro.estado);
            
            flujo.seek(0);
            c.numero_registros++;
            flujo.writeInt(c.numero_registros);
            flujo.writeInt(c.numero_vacios);
            
            flujo.close();
        } catch (IOException ex) {
            return -2;
        }
        return 1;
    }
    public static Registro leerRegistro(File fichero, int posicion){
        RandomAccessFile flujo;
        Registro r = new Registro();
        try{
            flujo = new RandomAccessFile(fichero, "r");
            flujo.seek(tama�o_cabecera + posicion*tama�o_registro);
            
            r.nro = flujo.readInt();
            r.item = flujo.readUTF();
            r.estado = flujo.readInt(); 
            
            flujo.close();
        }catch(IOException ex){
            return null;
        }
        return r;
    }
    public static DefaultListModel <String> leerTodos(File fichero){
        DefaultListModel <String> modelo = new DefaultListModel<>();
        RandomAccessFile flujo;
        try{
            flujo = new RandomAccessFile(fichero, "r");
            flujo.seek(tama�o_cabecera);
            
            int numR = numeroRegistros(flujo); 
            //System.out.println("num: " + numR);
            Registro c = new Registro();
            int i = 0;
            while(i < numR){
                flujo.seek(tama�o_cabecera + i*(tama�o_registro));
                c.nro = flujo.readInt();
                c.item = flujo.readUTF();
                c.estado = flujo.readInt();
                
                if(c.estado == 1){
                    modelo.addElement(c.item);
                }
                i++;
            }
            
            flujo.close();
        }
        catch(IOException ex){
            return null;
        }
        return modelo;
    }
    public static int eliminarRegistro(File fichero, int pos_registro){
        RandomAccessFile flujo;
        Registro r = new Registro();
        Cabecera c = new Cabecera();
        try{
            flujo = new RandomAccessFile(fichero, "rw");
            
            flujo.seek(tama�o_cabecera + pos_registro*tama�o_registro);
            r.nro = flujo.readInt();
            r.item = flujo.readUTF();
            r.estado = flujo.readInt(); 
            
            flujo.seek(tama�o_cabecera + pos_registro*tama�o_registro);
            r.estado = 0;
            flujo.writeInt(r.nro);
            flujo.writeUTF(r.item);
            flujo.writeInt(r.estado);
            
            flujo.seek(0);
            c.numero_registros = flujo.readInt();
            c.numero_vacios = flujo.readInt();
            
            flujo.seek(0);
            c.numero_registros--;
            c.numero_vacios++;
            flujo.writeInt(c.numero_registros);
            flujo.writeInt(c.numero_vacios);
            
            flujo.close();
        }catch(IOException ex){
            return -2;
        }
        return 1;
    }
    public static int buscar_Registro(File fichero, String item){
        int encontrado = -1;
        RandomAccessFile flujo;
        try {
            flujo = new RandomAccessFile(fichero, "rw");
            flujo.seek(tama�o_cabecera);
            String aux_item; int aux_nro, aux_estado;
            int i = 0, numR = numeroRegistros(flujo);
            while(i < numR){
                flujo.seek(tama�o_cabecera + i*(tama�o_registro));
                aux_nro  = flujo.readInt();
                aux_item = flujo.readUTF();
                aux_estado = flujo.readInt();
                
                if(aux_estado == 1){
                    if(aux_item.compareTo(item) == 0){
                        encontrado = aux_nro;
                        break;
                    }
                }
                
                i++;
            }
            
            flujo.close();
        } catch (IOException ex) {
           encontrado = -2;
        }
        return encontrado;
    }
    
    public static int numeroRegistros(RandomAccessFile flujo) 
            throws IOException{
        return (int) Math.ceil((double) (flujo.length() - 
                tama�o_cabecera) / (double) tama�o_registro);
    }
    
    public static class Cabecera{
        public int numero_registros;
        public int numero_vacios;
    }
    
    public static class Registro{
        public int nro;
        public String item;
        public int estado;
    }
}
