
package ficheros;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author pedroluis
 */

//manipulación de la estructura de una tabla
public class Fichero_E {
    public static long longitud_registro = 72; // bytes: longitud fija
    public static int escribirEstructura(File f, 
            HashMap<String, String> columnas,
            ArrayList <String> secuencia,
            String clave_primaria){
        RandomAccessFile flujo;
        try{
            flujo = new RandomAccessFile(f, "rw");
            boolean c = false; int i = 0, cp = 0;
            for (String clave : secuencia) {
                String columna = clave;
                String tipo = columnas.get(clave); 
                cp = 0;
                if(columna.compareTo(clave_primaria) == 0 && (!c)){
                    c = true;
                    cp = 1;
                }
                
                flujo.seek(i*longitud_registro);
                
                flujo.writeInt(i);
                flujo.writeUTF(columna);
                flujo.writeUTF(tipo);
                flujo.writeInt(cp);
                
                i++;
                System.out.println("entro " + i);
            }
            flujo.close();
        }catch (IOException ex){
            return -2;
        }
        return 1;
    }
    public static Object[][] leerColumasGUI(File f){
        Object[][] columnas;
        RandomAccessFile flujo;
        try{
            flujo = new RandomAccessFile(f, "r");
            int numero_registos = (int) Math.ceil((double) flujo.length()/ (double) longitud_registro);
            columnas = new Object[numero_registos][4];
            int i = 0;
            while(i < numero_registos){
                flujo.seek(i*longitud_registro);
                
                columnas[i][0] = ""+flujo.readInt();
                columnas[i][1] = flujo.readUTF();
                columnas[i][2] = flujo.readUTF();
                columnas[i][3] = flujo.readInt() == 1;
                
                /*System.out.println("ER: " + columnas[i][0] + " - " + columnas[i][1] 
                        + " - " + columnas[i][2] + " - " + columnas[i][3]);*/
                
                i++; 
            }
            flujo.close();
        }catch(IOException ex){
            return null;
        }
        return columnas;
    }
    
    public static class RegistroS {
        public int numero_registro;
        public String nombre_columna;
        public String tipo_dato;
        public int clave_primaria;
    }
}
