
package ficheros;

/**
 * @author pedroluis
 */

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

//manipulación de los datos de un tabla
public class Fichero_D {
    /**** fichero de indices ****/
    public static final long longitud_registro = 40; //longitud fija registro de indice
    
    public static int escribir_RegIndice(File f, Reg_Indice reg_i){
        RandomAccessFile flujo;
        try{
            flujo = new RandomAccessFile(f, "rw");
            int numero_registros = 
                    (int)Math.ceil((double)flujo.length()/(double)longitud_registro);
            if(numero_registros == 0){ 
                reg_i.numero_registro = 0;
                
                flujo.seek(0);
            }else{
                reg_i.numero_registro = numero_registros;
                
                flujo.seek(numero_registros * longitud_registro);     
            }
            
//            System.out.println("Nr: " + reg_i.numero_registro);
//            System.out.println("Cr: " + reg_i.clave_primaria);
//            System.out.println("Dr: " + reg_i.direccion_registro);
            
            flujo.writeInt(reg_i.numero_registro);
            flujo.writeUTF(reg_i.clave_primaria);
            flujo.writeLong(reg_i.direccion_registro);
            
            flujo.close();            
        }catch(IOException ex){
            return -2;
        }
        return 1;
    }
    
    public static ArrayList <Reg_Indice> leerTodosIndices(File f){
        RandomAccessFile flujo;
        ArrayList <Reg_Indice> indices = new ArrayList();
        try{
            flujo = new RandomAccessFile(f, "r");
            int numero_registros = 
                    (int)Math.ceil((double)flujo.length()/(double)longitud_registro);
            int i = 0;
            while(i < numero_registros){
                flujo.seek(i * longitud_registro);
                Reg_Indice reg_i = new Reg_Indice();
                
                reg_i.numero_registro = flujo.readInt();
                reg_i.clave_primaria = flujo.readUTF();
                reg_i.direccion_registro = flujo.readLong();
                
                if(reg_i.direccion_registro != -1){
                    indices.add(reg_i);
                }
                
                i++;
            }
            flujo.close();
        }catch(IOException ex){
            return null;
        }
        return indices;
    }
    
    public static int buscar_ClaveFI(File f,String clave){
        int s = -1;
        RandomAccessFile flujo;
        Reg_Indice reg_i = new Reg_Indice();
        try{
            flujo = new RandomAccessFile(f, "r");
            int numero_registros = 
                    (int)Math.ceil((double)flujo.length()/(double)longitud_registro);
            int i = 0;
            while(i < numero_registros){
                flujo.seek(i * longitud_registro);
                
                reg_i.numero_registro = flujo.readInt();
                reg_i.clave_primaria = flujo.readUTF();
                reg_i.direccion_registro = flujo.readLong();
                
                if(reg_i.clave_primaria.compareTo(clave) == 0){
                    return reg_i.numero_registro;
                }
                
                i++;
            }
            flujo.close();
        }
        catch(IOException ex){
            return -2;
        }
        return s;
    }
    
    public static class Reg_Indice{
        public int numero_registro;
        public String clave_primaria;
        public long direccion_registro;
    }
    
    /**** fichero de datos ****/
    public static long escribir_DatosFila(File f, Object[][] columnas,
            Object[] datos){
        RandomAccessFile flujo;long pos;
        try{
            flujo = new RandomAccessFile(f, "rw");
            int i = 0; String t;
            pos = flujo.length();
            flujo.seek(pos);
            for(Object dato : datos){
                t = columnas[i][2].toString();
                if(t.compareTo("CADENA") == 0){
                    flujo.writeUTF(dato.toString());
                }
                else if(t.compareTo("ENTERO") == 0){
                    flujo.writeInt((int)dato);
                }
                else if(t.compareTo("REAL") == 0){
                    flujo.writeDouble((double)dato);
                }
                else if(t.compareTo("BOOLEANO") == 0){
                    flujo.writeBoolean((boolean)dato);
                }
                i++;
            }
            
            flujo.close();
        }catch(IOException ex){
            return -2;
        }
        return pos;
    }
    
    public static Object[][] leerTodasFilas(File f, ArrayList <Reg_Indice> indices,
            Object[][] estructura){ // estructura[i][2]: tipo-dato
        Object[][] filas = new Object[0][0];
        RandomAccessFile flujo;
        try{
            flujo = new RandomAccessFile(f, "r");
            
            filas = new Object[indices.size()][estructura.length];
            int i = 0, j; String tipo;
            for(Reg_Indice reg_i : indices){
                flujo.seek(reg_i.direccion_registro);
                j = 0;
                while(j < estructura.length){
                    tipo = estructura[j][2].toString();

                    if(tipo.compareTo("CADENA") == 0){
                        filas[i][j] = flujo.readUTF();
                    }
                    else if(tipo.compareTo("ENTERO") == 0){
                        filas[i][j] = flujo.readInt();
                    }
                    else if(tipo.compareTo("REAL") == 0){
                        filas[i][j] = flujo.readDouble();
                    }
                    else if(tipo.compareTo("BOOLEANO") == 0){
                        filas[i][j] = flujo.readBoolean();
                    }
                    j++;
                }
                i++;
            }
 
            flujo.close();
        }catch(IOException ex){
            return null;
        }
        return filas;
    }
}