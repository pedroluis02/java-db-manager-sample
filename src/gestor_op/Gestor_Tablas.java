/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestor_op;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.DefaultListModel;
import ficheros.*;

/**
 *
 * @author pedroluis
 */

public class Gestor_Tablas {
    private String nombre_bd;
    private String dir_db_t;
    public Gestor_Tablas(){
    }
    public void setNombreBD(String nombre_bd){
        this.nombre_bd = nombre_bd;
        this.dir_db_t = Gestor_DB.DIRECTORIO_BD + "/" + nombre_bd; 
    }
    public String getNombreBD(){
        return nombre_bd;
    }
    
    public Gestor_DB.Respuesta_O crear(String nombre_tabla){
        
        File fichero = new File(dir_db_t + "/" + Gestor_DB.FICHERO_T);
        Gestor_DB.Respuesta_O res = new Gestor_DB.Respuesta_O();
        int s = Fichero_G.buscar_Registro(fichero, nombre_tabla);
        if(s != -1){
            res.estado = 0;
            res.mensaje = "Tabla ya existe";
        }
        else{
            res.estado = 1;            
            Gestor_DB.crearDirectorio(dir_db_t + "/" + nombre_tabla);
            Fichero_G.crear_vacio(new File(dir_db_t + "/" + 
                    nombre_tabla + "/" + nombre_tabla));
            Fichero_G.crear_vacio(new File(dir_db_t + "/" + 
                    nombre_tabla + "/" + Gestor_DB.FICHERO_ESTRUCTURA));
            Fichero_G.crear_vacio(new File(dir_db_t + "/" + 
                    nombre_tabla + "/" + Gestor_DB.FICHERO_INDICE));
            Fichero_G.Registro r = new Fichero_G.Registro();
            r.item = nombre_tabla;
            Fichero_G.escribirRegistro(fichero, r);
        }
        return res;
    }
    public Gestor_DB.Respuesta_O eliminar(String nombre_tabla){
        String dir_t = dir_db_t + "/" + Gestor_DB.FICHERO_T;
        File fichero = new File(dir_t);
        Gestor_DB.Respuesta_O res = new Gestor_DB.Respuesta_O();
        int s = Fichero_G.buscar_Registro(fichero, nombre_tabla);
        if(s == -1){
            res.estado = 0;
            res.mensaje = "Tabla no existe";
        }
        else{
            res.estado = 1;
            Gestor_DB.eliminarDirectorio(dir_db_t + "/" + nombre_tabla);
            Fichero_G.eliminarRegistro(fichero, s);
        }
        return res;  
    }
    public DefaultListModel mostrar(){
        DefaultListModel modelo = Fichero_G.leerTodos(new File(dir_db_t 
                + "/" + Gestor_DB.FICHERO_T));
        return modelo;
    }
    
    public int insertarEstructura(String nombre_tabla, 
            HashMap<String, String> columnas, ArrayList <String> secuencia,
            String clave_primaria){
        String dir_s_t = dir_db_t + "/" + nombre_tabla + "/"+ Gestor_DB.FICHERO_ESTRUCTURA;
        File f = new File(dir_s_t);
        return Fichero_E.escribirEstructura(f, columnas, secuencia, clave_primaria);
    }
    
    public Object[][] mostrarColumnasGui(String nombre_tabla){
        String dir_s_t = dir_db_t + "/" + nombre_tabla + "/"+ Gestor_DB.FICHERO_ESTRUCTURA;
        File f = new File(dir_s_t);
        return Fichero_E.leerColumasGUI(f);
    }
    
    //DML
    
    public Gestor_DB.Respuesta_O insertar_DatosFila(String nombre_tabla, Object[][] estructura,
            Object[] datos){
        
        int clave = -1;
        int k = 0;
        while(k < estructura.length){
            if((boolean)estructura[k][3] == true){
                clave = k;
                break;
            }
            k++;
        }
        
        String dir_i_t = dir_db_t + "/" + nombre_tabla + "/"
                + Gestor_DB.FICHERO_INDICE;
        File fi = new File(dir_i_t);
        Gestor_DB.Respuesta_O res = new Gestor_DB.Respuesta_O();
        res.estado = 0;
        int s = Fichero_D.buscar_ClaveFI(fi, datos[clave].toString());
        if(s != -1){
            res.mensaje = "Clave '" + datos[clave].toString() + "' duplicada";
            return res;
        }
        
        String dir_d_t = dir_db_t + "/" + nombre_tabla + "/"
                + nombre_tabla;
        File ff = new File(dir_d_t);
        long rf = Fichero_D.escribir_DatosFila(ff, estructura, datos);
        if(rf == -2){
            res.mensaje = "Error al insertar Datos";
            return res;
        }
        
        Fichero_D.Reg_Indice reg_i = new Fichero_D.Reg_Indice();
        reg_i.clave_primaria = datos[clave].toString();
        reg_i.direccion_registro = rf;
        
        int ri = Fichero_D.escribir_RegIndice(fi, reg_i);
        
        if(ri == -2){
            res.mensaje = "Error al insertar en Indices";
            return res;
        }
        
        res.estado = 1;
        return res;
    }
    
    public Object[][] obtenerTodosDatos(String nombre_tabla, Object[][] estructura){
        String dir_i_t = dir_db_t + "/" + nombre_tabla + "/" 
                + Gestor_DB.FICHERO_INDICE;
        File fi = new File(dir_i_t);
        ArrayList <Fichero_D.Reg_Indice> indices = Fichero_D.leerTodosIndices(fi);
        if(indices.isEmpty()){
            return new Object[][]{};
        }
        
        String dir_d_t = dir_db_t + "/" + nombre_tabla + "/" + nombre_tabla;
        File fd = new File(dir_d_t);
        return Fichero_D.leerTodasFilas(fd, indices, estructura);
    }
    
    public int existeClave(String nombre_tabla, String clave){
        String dir_i_t = dir_db_t + "/" + nombre_tabla + "/" + Gestor_DB.FICHERO_INDICE;
        File fi = new File(dir_i_t);
        return Fichero_D.buscar_ClaveFI(fi, clave);
    }
}
