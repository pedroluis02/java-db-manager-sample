/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestor_op;

import java.io.File;
import javax.swing.DefaultListModel;
import ficheros.Fichero_G;

/**
 *
 * @author pedroluis
 */

//operaciones sobre una base de datos
public class Gestor_DB {
    
    public static final String DIR = System.getProperty("user.dir");
    public static final String DIRECTORIO_BD = DIR + "/" + "DatosBD";
    public static final String FICHERO_BD = "basedatos";
    public static final String DIR_F_BD = DIRECTORIO_BD + "/" + FICHERO_BD;
    public static final String FICHERO_T = "tablas";
    public static final String FICHERO_ESTRUCTURA = "estructura";
    public static final String FICHERO_INDICE = "indice";
    public static Respuesta_O Respuesta_O;
    
    //crear
    public static Respuesta_O crear(String nombre_bd){
        String dir_bd = DIRECTORIO_BD + "/" + nombre_bd;
        
        File fichero = new File(DIR_F_BD);
        Respuesta_O res = new Respuesta_O();
        int s = Fichero_G.buscar_Registro(fichero, nombre_bd);
        if(s != -1){
            res.estado = 0;
            res.mensaje = "BD ya existe";
        }
        else{
            res.estado = 1;            
            crearDirectorio(dir_bd);
            Fichero_G.crear(new File(dir_bd + "/" + FICHERO_T));
            Fichero_G.Registro r = new Fichero_G.Registro();
            r.item = nombre_bd;
            Fichero_G.escribirRegistro(fichero, r);
        }
        return res;
    }
    //eliminar
    public static Respuesta_O eliminar(String nombre_bd){
        File fichero = new File(DIR_F_BD);
        Respuesta_O res = new Respuesta_O();
        int s = Fichero_G.buscar_Registro(fichero, nombre_bd);
        if(s == -1){
            res.estado = 0;
            res.mensaje = "BD no existe";
        }
        else{
            res.estado = 1;
            Fichero_G.eliminarRegistro(fichero, s);
            Gestor_DB.eliminarDirectorio_BD(DIRECTORIO_BD + "/" + nombre_bd);
        }
        return res;        
    }
    //mostrar todas la base de datos creadas
    public static DefaultListModel mostrar(){
        DefaultListModel modelo = 
                Fichero_G.leerTodos(new File(DIR_F_BD));
        return modelo;
    }
    
    public static void eliminarDirectorio_BD(String dir_bd){
        File dir = new File(dir_bd);
        for(File fd : dir.listFiles()){
            if(fd.isDirectory()){
                eliminarDirectorio(dir_bd + "/" + fd.getName());
            }
            else{
                fd.delete();
            }
        }
        dir.delete();
    }
    
    public static void eliminarDirectorio(String dir){
        File d = new File(dir);
        for(File f : d.listFiles()){
           f.delete(); 
        }
        d.delete();
    }
    
    public static int crearDirectorio(String dir){
        File f = new File(dir);
        f.mkdir();
        return 1;
    }
    
    public static class Respuesta_O{
        public int estado;
        public String mensaje;
    }
}
