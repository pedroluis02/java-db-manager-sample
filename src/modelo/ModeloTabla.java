/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author pedroluis
 */

public class ModeloTabla extends DefaultTableModel{
    private Class[] tipos;
    private String[] nombres;
    private boolean[] editables;
    
    public ModeloTabla(Object[][] estructura, Object[][] datos,
            boolean editable_todo){
        convertiraClass(estructura, editable_todo);
        super.setDataVector(datos, nombres);
    }
    private void convertiraClass(Object[][] estructura, boolean editable_todo){
        tipos = new Class[estructura.length];
        nombres = new String[estructura.length];
        editables = new boolean[estructura.length];
        
        int i = 0; String t;
        while(i < estructura.length) {
            t = estructura[i][2].toString();
            
            if(t.compareTo("CADENA") == 0){
                tipos[i] = String.class;
            }
            else if(t.compareTo("ENTERO") == 0){
                tipos[i] = Integer.class;
            }
            else if(t.compareTo("REAL") == 0){
                tipos[i] = Double.class;
            }
            else if(t.compareTo("BOOLEANO") == 0){
                tipos[i] = Boolean.class;
            }
            
            nombres[i] = estructura[i][1].toString();
            editables[i] = editable_todo;
            i++;
        }
    }
    
    @Override
    public Class getColumnClass(int columna){
        return tipos[columna];
    }
    
    @Override
    public boolean isCellEditable(int fila, int columna) {
        return editables[columna];
    }
    
    public String[] getNombres_columnas(){
        return nombres;
    }
}
