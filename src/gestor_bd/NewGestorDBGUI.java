
package gestor_bd;

import gestor_op.Gestor_DB;
import gestor_op.Gestor_Tablas;
import java.io.File;
import java.util.HashMap;
import javax.swing.DefaultListModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import ficheros.Fichero_G;

/**
 *
 * @author pedroluis
 */

public class NewGestorDBGUI extends javax.swing.JFrame {

    private HashMap <String, DefaultMutableTreeNode> dbs;
    public NewGestorDBGUI() {
        initComponents();
        mostrarBD();
    }
    
    private void mostrarBD() {
        DefaultListModel <String> list = Gestor_DB.mostrar();
        dbs = new HashMap<>();
        int i = 0;
        DefaultMutableTreeNode raiz = new DefaultMutableTreeNode("Base de Datos");
        DefaultTreeModel model = new DefaultTreeModel( raiz );
        while(i < list.size()) {
            DefaultMutableTreeNode nodo = new DefaultMutableTreeNode( list.get(i) );
            raiz.add(nodo);
            nodo.add(new DefaultMutableTreeNode());
            dbs.put(list.get(i), nodo);
            i++;
        }
        jtreeBaseDatos.setModel(model);
    }
    
    private void mostrarTablas(String nombre_bd) {
        DefaultMutableTreeNode nodo = dbs.get( nombre_bd );
        Gestor_Tablas t = new Gestor_Tablas();
        t.setNombreBD( nombre_bd  );
        int i = 0;
        DefaultListModel <String> m = t.mostrar();
        nodo.removeAllChildren();
        while(i < m.size()) {
            DefaultMutableTreeNode nodo_t = new DefaultMutableTreeNode( m.get(i) );
            nodo_t.add(new DefaultMutableTreeNode());
            nodo.add( nodo_t);
            
            i++;
        }
        
    }
    
    private void mostrarColumnas(String nombre_bd, String nombre_tb) {
        DefaultMutableTreeNode nodo = dbs.get( nombre_bd );
        Gestor_Tablas t = new Gestor_Tablas();
        t.setNombreBD( nombre_bd  );
        int i = 0;
        DefaultListModel <String> m = t.mostrar();
        nodo.removeAllChildren();
        while(i < m.size()) {
            DefaultMutableTreeNode nodo_t = new DefaultMutableTreeNode( m.get(i) );
            nodo_t.add(new DefaultMutableTreeNode());
            nodo.add( nodo_t);
            
            i++;
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jtreeBaseDatos = new javax.swing.JTree();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Base de Datos");
        jtreeBaseDatos.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        jtreeBaseDatos.addTreeWillExpandListener(new javax.swing.event.TreeWillExpandListener() {
            public void treeWillCollapse(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
            }
            public void treeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
                jtreeBaseDatosTreeWillExpand(evt);
            }
        });
        jtreeBaseDatos.addTreeExpansionListener(new javax.swing.event.TreeExpansionListener() {
            public void treeCollapsed(javax.swing.event.TreeExpansionEvent evt) {
            }
            public void treeExpanded(javax.swing.event.TreeExpansionEvent evt) {
                jtreeBaseDatosTreeExpanded(evt);
            }
        });
        jtreeBaseDatos.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                jtreeBaseDatosValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jtreeBaseDatos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(285, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtreeBaseDatosValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_jtreeBaseDatosValueChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_jtreeBaseDatosValueChanged

    private void jtreeBaseDatosTreeExpanded(javax.swing.event.TreeExpansionEvent evt) {//GEN-FIRST:event_jtreeBaseDatosTreeExpanded
//
//        TreePath tp = evt.getPath();
//        System.out.println( tp.getPath()[1].toString());
    }//GEN-LAST:event_jtreeBaseDatosTreeExpanded

    private void jtreeBaseDatosTreeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {//GEN-FIRST:event_jtreeBaseDatosTreeWillExpand
        // TODO add your handling code here:
        TreePath tp = evt.getPath();
        int contador = tp.getPathCount();
        if(contador == 1) {
            mostrarBD();
        } else if(contador == 2) {
            mostrarTablas(  tp.getPathComponent(1).toString() );
        }
        
    }//GEN-LAST:event_jtreeBaseDatosTreeWillExpand

    public static void main(String args[]) {
         try {
            for (javax.swing.UIManager.LookAndFeelInfo info : 
                    javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | 
                InstantiationException | 
                IllegalAccessException | 
                javax.swing.UnsupportedLookAndFeelException ex) {
        }
                
        File f = new File(Gestor_DB.DIRECTORIO_BD);

        if(f.exists()){
            System.out.println("Dir DB ya esta creado.");
        }
        else{
            Gestor_DB.crearDirectorio(Gestor_DB.DIRECTORIO_BD);
            System.out.println("Sre creo dir: " + Gestor_DB.DIRECTORIO_BD);
        }

        File d = new File(Gestor_DB.DIR_F_BD);
        if(d.exists()){
            System.out.println("Fichero de DB ya esta creado.");
        }
        else{
            Fichero_G.crear(d);
            System.out.println("Sre creo dir: " + Gestor_DB.DIR_F_BD);
        }
        NewGestorDBGUI g = new NewGestorDBGUI();
        g.setLocationRelativeTo(null);
        g.setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTree jtreeBaseDatos;
    // End of variables declaration//GEN-END:variables
}
