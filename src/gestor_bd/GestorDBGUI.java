
package gestor_bd;

import gestor_op.Gestor_DB;
import gestor_op.Gestor_Tablas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.event.MouseInputAdapter;
import ficheros.Fichero_G;

/**
 * @author pedroluis
 */
public class GestorDBGUI extends javax.swing.JFrame{

    public GestorDBGUI() {
        initComponents();
        jlistBaseDatos.setModel(Gestor_DB.mostrar());
        
        columnasDefinicion = new Def_Table(this, false);
        columnasVer = new Des_Table(this, false);
        
        mostrarDatos = new Dat_Table(this, false);
        insertarDatos = new Op_Table(this, false);
        
        crearPopupMenuTabla();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jbEliminarBD = new javax.swing.JButton();
        jbActualizarBD = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jlistBaseDatos = new javax.swing.JList();
        jbCrearDB = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jbSalir = new javax.swing.JButton();
        jpTablas = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jtfDBSeleccionada = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jlistTablas = new javax.swing.JList();
        jbCrearTabla = new javax.swing.JButton();
        jbEliminarTabla = new javax.swing.JButton();
        jbActualizarTablas = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jtfNombreTabla = new javax.swing.JTextField();
        jbVerEstructura = new javax.swing.JButton();
        jbVerDatos = new javax.swing.JButton();
        jbInsertarDatos = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jmiConsola = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jmiSalir = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestor BD");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Base de Datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 12), java.awt.Color.black)); // NOI18N

        jbEliminarBD.setText("Eliminar");
        jbEliminarBD.setEnabled(false);
        jbEliminarBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbEliminarBDActionPerformed(evt);
            }
        });

        jbActualizarBD.setText("Actualizar BD");
        jbActualizarBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbActualizarBDActionPerformed(evt);
            }
        });

        jlistBaseDatos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jlistBaseDatos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jlistBaseDatosValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jlistBaseDatos);

        jbCrearDB.setText("Crear");
        jbCrearDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCrearDBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jbCrearDB, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbEliminarBD, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE))
                    .addComponent(jbActualizarBD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbEliminarBD)
                    .addComponent(jbCrearDB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbActualizarBD)
                .addContainerGap())
        );

        jPanel5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jbSalir.setText("Salir");
        jbSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSalirActionPerformed(evt);
            }
        });
        jPanel5.add(jbSalir);

        jpTablas.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tablas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 12), java.awt.Color.black)); // NOI18N

        jLabel3.setText("BD Seleccionada");

        jtfDBSeleccionada.setEditable(false);

        jlistTablas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jlistTablas.setEnabled(false);
        jlistTablas.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jlistTablasValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jlistTablas);

        jbCrearTabla.setText("Crear");
        jbCrearTabla.setEnabled(false);
        jbCrearTabla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCrearTablaActionPerformed(evt);
            }
        });

        jbEliminarTabla.setText("Eiminar");
        jbEliminarTabla.setEnabled(false);
        jbEliminarTabla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbEliminarTablaActionPerformed(evt);
            }
        });

        jbActualizarTablas.setText("Actualizar Tablas");
        jbActualizarTablas.setEnabled(false);
        jbActualizarTablas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbActualizarTablasActionPerformed(evt);
            }
        });

        jLabel4.setText("Nombre de Tabla");

        jtfNombreTabla.setEnabled(false);

        jbVerEstructura.setText("Descripci�n");
        jbVerEstructura.setEnabled(false);
        jbVerEstructura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbVerEstructuraActionPerformed(evt);
            }
        });

        jbVerDatos.setText("Ver Datos");
        jbVerDatos.setEnabled(false);
        jbVerDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbVerDatosActionPerformed(evt);
            }
        });

        jbInsertarDatos.setText("Insertar Datos");
        jbInsertarDatos.setEnabled(false);
        jbInsertarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbInsertarDatosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpTablasLayout = new javax.swing.GroupLayout(jpTablas);
        jpTablas.setLayout(jpTablasLayout);
        jpTablasLayout.setHorizontalGroup(
            jpTablasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpTablasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpTablasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jbCrearTabla, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jbEliminarTabla, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jbActualizarTablas, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)
                    .addGroup(jpTablasLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jbVerEstructura, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jbVerDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jbInsertarDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jpTablasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                    .addComponent(jtfDBSeleccionada)
                    .addComponent(jtfNombreTabla))
                .addContainerGap())
        );
        jpTablasLayout.setVerticalGroup(
            jpTablasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpTablasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpTablasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jtfDBSeleccionada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jpTablasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jtfNombreTabla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpTablasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpTablasLayout.createSequentialGroup()
                        .addComponent(jbCrearTabla)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbVerEstructura)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbInsertarDatos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbVerDatos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 178, Short.MAX_VALUE)
                        .addComponent(jbEliminarTabla)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbActualizarTablas))
                    .addComponent(jScrollPane1)))
        );

        jMenu1.setText("Aplicaci�n");
        jMenu1.setMnemonic('A');

        jmiConsola.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jmiConsola.setText("Consola");
        jmiConsola.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConsolaActionPerformed(evt);
            }
        });
        jMenu1.add(jmiConsola);
        jMenu1.add(jSeparator1);

        jmiSalir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jmiSalir.setText("Salir");
        jmiSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSalirActionPerformed(evt);
            }
        });
        jMenu1.add(jmiSalir);

        jMenuBar1.add(jMenu1);

        jMenu3.setText("Ayuda");

        jMenuItem3.setText("Acerca de ...");
        jMenu3.add(jMenuItem3);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jpTablas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jpTablas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel4.getAccessibleContext().setAccessibleName("Bases de Datos Creadas");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //crear un base de datos
    private void jbCrearDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCrearDBActionPerformed
        // TODO add your handling code here:
        String nombre_bd = JOptionPane.showInputDialog(GestorDBGUI.this, "Nombre:", "Crear nueva Base de Datos", JOptionPane.NO_OPTION);
        
        if(nombre_bd == null) {
            return;
        }
        
        if( nombre_bd.length() == 0){
            mostrarMensaje(JOptionPane.ERROR_MESSAGE, "Nombre BD no es v�lido.");
            return;
        }
        else if(!Character.isAlphabetic(nombre_bd.charAt(0))){
            mostrarMensaje(JOptionPane.ERROR_MESSAGE, "Nombre BD no es v�lido(Debe inciar con una letra).");
            return;
        }
        
        Gestor_DB.Respuesta_O res = Gestor_DB.crear(nombre_bd);
        
        if(res.estado == 0){
            mostrarMensaje(JOptionPane.WARNING_MESSAGE, res.mensaje);
        }
    }//GEN-LAST:event_jbCrearDBActionPerformed

    private void jbActualizarBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbActualizarBDActionPerformed
        // TODO add your handling code here:
        DefaultListModel modelo = Gestor_DB.mostrar();
        if(modelo != null){
            jlistBaseDatos.setModel(modelo);
        }
    }//GEN-LAST:event_jbActualizarBDActionPerformed

    private void jbCrearTablaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCrearTablaActionPerformed
        // TODO add your handling code here:
        String nombre_tabla = jtfNombreTabla.getText();
        
        if( nombre_tabla.length() == 0){
            mostrarMensaje(JOptionPane.ERROR_MESSAGE, "Nombre Tabla no es v�lido.");
            return;
        }
        else if(!Character.isAlphabetic(nombre_tabla.charAt(0))){
            mostrarMensaje(JOptionPane.ERROR_MESSAGE, "Nombre Tabla no es v�lido(Debe inciar con una letra).");
            return;
        }
        columnasDefinicion.setLocationRelativeTo(this);
        columnasDefinicion.show();
    }//GEN-LAST:event_jbCrearTablaActionPerformed

    public void estructuraTabla(){
        if(columnasDefinicion.getClavePrimaria().isEmpty()){
            return;
        }
        
        String nombre_tabla = jtfNombreTabla.getText();
        
        Gestor_DB.Respuesta_O res = tablas_op.crear(nombre_tabla);
        if(res.estado == 0){
            mostrarMensaje(JOptionPane.WARNING_MESSAGE, res.mensaje);
            return;
        }
        
        int rpta = tablas_op.insertarEstructura(nombre_tabla, 
                columnasDefinicion.getColumnas(), 
                columnasDefinicion.getSecuencia(),
                columnasDefinicion.getClavePrimaria());
        
        columnasDefinicion.clearVariables();
    }
    
    private void jbActualizarTablasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbActualizarTablasActionPerformed
        // TODO add your handling code here:
        tablas_op.setNombreBD(jtfDBSeleccionada.getText());
        DefaultListModel modelo = tablas_op.mostrar();
        jlistTablas.setModel(modelo);
    }//GEN-LAST:event_jbActualizarTablasActionPerformed

    private void jbVerEstructuraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbVerEstructuraActionPerformed
        // TODO add your handling code here:
       Object[][] data_aux = tablas_op.mostrarColumnasGui(jlistTablas.getSelectedValue().toString());
       if(data_aux == null){
           mostrarMensaje(JOptionPane.ERROR_MESSAGE, "Error al Abrir mostrar estructura.");
           return;
       }
       columnasVer.setDatosColumas(jlistTablas.getSelectedValue().toString(), data_aux);
       columnasVer.setLocationRelativeTo(this);
       columnasVer.show();
    }//GEN-LAST:event_jbVerEstructuraActionPerformed

    private void jlistBaseDatosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jlistBaseDatosValueChanged
        // TODO add your handling code here:
        int index = jlistBaseDatos.getSelectedIndex();
        if(index != -1){
            jbEliminarBD.setEnabled(true);
            
            jlistTablas.setEnabled(true);
            jtfNombreTabla.setEnabled(true);
            jbCrearTabla.setEnabled(true);
            jbActualizarTablas.setEnabled(true);
            
            tablas_op.setNombreBD(jlistBaseDatos.getSelectedValue().toString()); 
            jtfDBSeleccionada.setText(tablas_op.getNombreBD());
            jlistTablas.setModel(tablas_op.mostrar());
        }
        else{
            jbEliminarBD.setEnabled(false);
            
            jlistTablas.setEnabled(false);
            jtfNombreTabla.setEnabled(false);
            jbCrearTabla.setEnabled(false);
            jbActualizarTablas.setEnabled(false);
            
            jlistTablas.setModel(new DefaultListModel());
            jtfDBSeleccionada.setText("");
            jtfNombreTabla.setText("");
        }
    }//GEN-LAST:event_jlistBaseDatosValueChanged

    private void jlistTablasValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jlistTablasValueChanged
        // TODO add your handling code here:
        int index = jlistTablas.getSelectedIndex();
        if(index != -1){
            jbInsertarDatos.setEnabled(true);
            jbVerDatos.setEnabled(true);
            jbEliminarTabla.setEnabled(true);
            jbVerEstructura.setEnabled(true);
        }else{
            jbInsertarDatos.setEnabled(false);
            jbVerDatos.setEnabled(false);
            jbEliminarTabla.setEnabled(false);
            jbVerEstructura.setEnabled(false);
        }
    }//GEN-LAST:event_jlistTablasValueChanged

    private void jbEliminarBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbEliminarBDActionPerformed
        // TODO add your handling code here:
        String bd_seleccionada = jlistBaseDatos.getSelectedValue().toString();
        int rpta = JOptionPane.showConfirmDialog(this, 
                "�Desea eliminar DB: '" + bd_seleccionada + "'?",
                "Eliminar Base de Datos", JOptionPane.OK_CANCEL_OPTION);
        if(rpta == JOptionPane.OK_OPTION) {
            Gestor_DB.Respuesta_O res = Gestor_DB.eliminar(bd_seleccionada);
            if(res.estado == 0){
                mostrarMensaje(JOptionPane.WARNING_MESSAGE, res.mensaje);
            }
            else{
                jlistBaseDatos.setModel(Gestor_DB.mostrar());
            }
        }
    }//GEN-LAST:event_jbEliminarBDActionPerformed

    private void jbEliminarTablaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbEliminarTablaActionPerformed
        // TODO add your handling code here:
        String tabla_seleccionada = jlistTablas.getSelectedValue().toString();
        int rpta = JOptionPane.showConfirmDialog(this, 
                "�Desea eliminar la Tabla: '" + tabla_seleccionada + "'?",
                "Eliminar Tabla", JOptionPane.OK_CANCEL_OPTION);
        if(rpta == JOptionPane.OK_OPTION) {
            Gestor_DB.Respuesta_O res = tablas_op.eliminar(tabla_seleccionada);
            if(res.estado == 0){
                mostrarMensaje(JOptionPane.WARNING_MESSAGE, res.mensaje);
            }
            else{
                jlistTablas.setModel(tablas_op.mostrar());
            }
        }
    }//GEN-LAST:event_jbEliminarTablaActionPerformed

    private void jbSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbSalirActionPerformed
        // TODO add your handling code here:
        int rpta = JOptionPane.showConfirmDialog(this, 
                "�Desea Salir de GestorDBGUI?", "Cerrar GestoDBGUi",
                JOptionPane.OK_CANCEL_OPTION);
        if(rpta == JOptionPane.OK_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_jbSalirActionPerformed

    private void jbVerDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbVerDatosActionPerformed
        // TODO add your handling code here:
        Object[][] estructura = 
                tablas_op.mostrarColumnasGui(
                jlistTablas.getSelectedValue().toString());
        
        
        Object[][] datos = tablas_op.obtenerTodosDatos(
                jlistTablas.getSelectedValue().toString(), estructura);
        
        if(datos == null){
            mostrarMensaje(JOptionPane.ERROR_MESSAGE, 
                    "Error al intentar obtener datos");
            return;
        }
        
        if(datos.length == 0){
            mostrarMensaje(JOptionPane.WARNING_MESSAGE, "Tabla '" + 
                    jlistTablas.getSelectedValue().toString() 
                    + "' no contiene datos");
            return;
        }
        
        mostrarDatos.setDatos(estructura, datos);
        mostrarDatos.setLocationRelativeTo(this);
        mostrarDatos.show();
    }//GEN-LAST:event_jbVerDatosActionPerformed

    private void jbInsertarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbInsertarDatosActionPerformed
        // TODO add your handling code here:
        String nombre_tabla = jlistTablas.getSelectedValue().toString();
        Object[][] estructura = tablas_op.mostrarColumnasGui(nombre_tabla);
        
        //obtener datos
        //Object datos[];
        
        insertarDatos.setEstructuraTabla(nombre_tabla, estructura);
        insertarDatos.setLocationRelativeTo(this);
        insertarDatos.show();
    }//GEN-LAST:event_jbInsertarDatosActionPerformed

    private void jmiSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSalirActionPerformed
        int rpta = JOptionPane.showConfirmDialog(this, 
                "Desea Salir de GestorDBGUI", "Cerrar GestoDBGUi",
                JOptionPane.OK_CANCEL_OPTION);
        if(rpta == JOptionPane.OK_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_jmiSalirActionPerformed

    private void jmiConsolaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConsolaActionPerformed

        JOptionPane.showMessageDialog(this, "En Desarrollo...");
    }//GEN-LAST:event_jmiConsolaActionPerformed

    /**
     * @param args the command line arguments
     */
    
    private void mostrarMensaje(int tipo, String mensaje){
        switch(tipo){
            case JOptionPane.WARNING_MESSAGE:
                JOptionPane.showMessageDialog(this, mensaje, "Warning", tipo);
                break;
            case JOptionPane.ERROR_MESSAGE:
                JOptionPane.showMessageDialog(this, mensaje, "Error", tipo);
                break;
            case JOptionPane.INFORMATION_MESSAGE:
                JOptionPane.showMessageDialog(this, mensaje, "Mensaje", tipo);
                break;
        }
        
    }
    
    private void crearPopupMenuTabla(){
        popupMenuTablas = new JPopupMenu();
        popupMenuTablas.setBorderPainted(true);
        menuItems = new JMenuItem[4];
        
        menuItems[0] = new JMenuItem("Ver Estructura");
        menuItems[0].setActionCommand("descripcion");
        menuItems[1] = new JMenuItem("Insertar Datos");
        menuItems[1].setActionCommand("insertar");
        menuItems[2] = new JMenuItem("Ver Datos");
        menuItems[2].setActionCommand("datos");
        menuItems[3] = new JMenuItem("Eliminar");
        menuItems[3].setActionCommand("eliminar");
        
        popupMenuTablas.add(menuItems[0]);
        popupMenuTablas.add(menuItems[1]);
        popupMenuTablas.add(menuItems[2]);
        popupMenuTablas.addSeparator();
        popupMenuTablas.add(menuItems[3]);
        
        jlistTablas.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mousePressed(MouseEvent evt){
                if(!evt.isPopupTrigger()) {
                    return;
                }
                
                int fila_seleccionada = jlistTablas.getSelectedIndex();
                if(fila_seleccionada != -1){
                    popupMenuTablas.show(jlistTablas, 
                            evt.getX(), evt.getY());
                }
            }
        });
        
        ActionListener al = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(ae.getActionCommand().compareTo("descripcion") == 0){
                    jbVerEstructuraActionPerformed(ae);
                }
                else if(ae.getActionCommand().compareTo("insertar") == 0){
                    jbInsertarDatosActionPerformed(ae);
                }
                else if(ae.getActionCommand().compareTo("datos") == 0){
                    jbVerDatosActionPerformed(ae);
                }
                else if(ae.getActionCommand().compareTo("eliminar") == 0){
                    jbEliminarTablaActionPerformed(ae);
                }
            }
        };
        
        menuItems[0].addActionListener(al);
        menuItems[1].addActionListener(al);
        menuItems[2].addActionListener(al);
        menuItems[3].addActionListener(al);
        
    }
    
    public int existeClaveI(String nombre_tabla, String clave){
        return tablas_op.existeClave(nombre_tabla, clave);
    }
    
    public Gestor_DB.Respuesta_O insertarDatosGui(String nombre_tabla,
            Object[][] estructura, Object[] datos){
        return tablas_op.insertar_DatosFila(nombre_tabla, estructura, datos);
    }
    
    public static void main(String args[]) {
         try {
            for (javax.swing.UIManager.LookAndFeelInfo info : 
                    javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | 
                InstantiationException | 
                IllegalAccessException | 
                javax.swing.UnsupportedLookAndFeelException ex) {
        }
                
        File f = new File(Gestor_DB.DIRECTORIO_BD);

        if(f.exists()){
            System.out.println("Dir DB ya esta creado.");
        }
        else{
            Gestor_DB.crearDirectorio(Gestor_DB.DIRECTORIO_BD);
            System.out.println("Sre creo dir: " + Gestor_DB.DIRECTORIO_BD);
        }

        File d = new File(Gestor_DB.DIR_F_BD);
        if(d.exists()){
            System.out.println("Fichero de DB ya esta creado.");
        }
        else{
            Fichero_G.crear(d);
            System.out.println("Sre creo dir: " + Gestor_DB.DIR_F_BD);
        }
        GestorDBGUI g = new GestorDBGUI();
        g.setLocationRelativeTo(null);
        g.setVisible(true);
    }
    
    private Gestor_Tablas tablas_op 
            = new Gestor_Tablas();
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JButton jbActualizarBD;
    private javax.swing.JButton jbActualizarTablas;
    private javax.swing.JButton jbCrearDB;
    private javax.swing.JButton jbCrearTabla;
    private javax.swing.JButton jbEliminarBD;
    private javax.swing.JButton jbEliminarTabla;
    private javax.swing.JButton jbInsertarDatos;
    private javax.swing.JButton jbSalir;
    private javax.swing.JButton jbVerDatos;
    private javax.swing.JButton jbVerEstructura;
    private javax.swing.JList jlistBaseDatos;
    private javax.swing.JList jlistTablas;
    private javax.swing.JMenuItem jmiConsola;
    private javax.swing.JMenuItem jmiSalir;
    private javax.swing.JPanel jpTablas;
    private javax.swing.JTextField jtfDBSeleccionada;
    private javax.swing.JTextField jtfNombreTabla;
    // End of variables declaration//GEN-END:variables

    private Def_Table columnasDefinicion;
    private Des_Table columnasVer;
    
    private Dat_Table mostrarDatos;
    private Op_Table insertarDatos;
    
    private JPopupMenu popupMenuTablas;
    private JMenuItem menuItems[];
}
